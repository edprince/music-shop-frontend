import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  useHistory
} from "react-router-dom";
import Nav from './components/layout/Nav';
import Footer from './components/layout/Footer';
import Store from './components/pages/Store';
import About from './components/pages/About';
import Product from './components/pages/Product';
import Blog from './components/pages/Blog';
import Checkout from './components/pages/Checkout';
import Signup from './components/pages/Signup';
import Blogpost from './components/pages/Blogpost';
import { SearchProvider } from './context/SearchContext';
import {cartProvider, CartProvider } from './context/CartContext';

function App() {
  return (
    <div className="bg-gray-100">
      <Router>
        <Nav />
        <div className="container px-5 py-8 mx-auto">
        <Switch>
          <Route exact path="/">
            <SearchProvider>
              <Store />
            </SearchProvider>
          </Route>
          <Route path="/about">
            <About />
          </Route>
          <Route path="/product">
            <Product />
          </Route>
          <Route path="/blog">
            <Blog />
          </Route>
          <PrivateRoute path="/checkout">
            <CartProvider>
              <Checkout />
            </CartProvider>
          </PrivateRoute>
          <Route path="/signup">
            <Signup auth={fakeAuth}/>
          </Route>
          <Route path="/blogpost">
            <Blogpost />
          </Route>
        </Switch>
        </div>
      <Footer />
      </Router>
    </div>
  );
}


const fakeAuth = {
  isAuthenticated: false,
  authenticate(cb) {
    fakeAuth.isAuthenticated = true;
    setTimeout(cb, 100); // fake async
  },
  signout(cb) {
    fakeAuth.isAuthenticated = false;
    setTimeout(cb, 100);
  }
};


function PrivateRoute({ children, ...rest }) {
  return (
    <Route
      {...rest}
      render={() =>
        fakeAuth.isAuthenticated ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/signup"
            }}
          />
        )
      }
    />
  );
}

export default App;
