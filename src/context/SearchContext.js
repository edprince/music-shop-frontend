import React, { useState, createContext } from 'react';

export const SearchContext = createContext();

export const SearchProvider = (props) => {
    const [filter, setFilter] = useState("all");
    return(
        <SearchContext.Provider value={[filter, setFilter]}>
            {props.children}
        </SearchContext.Provider>
    );
}