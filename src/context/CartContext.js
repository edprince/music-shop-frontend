import React, { useState, createContext } from 'react';

export const CartContext = createContext();

export const CartProvider = (props) => {
    const [cart, setCart] = useState([
        {
            id: "a8fh2jd9903jhdoc98",
            filesize: "2.8MB",
            url: "https://dt7v1i9vyp3mf.cloudfront.net/styles/news_large/s3/imagelibrary/N/NativeInstrumentsMassiveX_01-9kyL90Cf9j6d9HzeOTId2Pga.kDmMrkL.jpg",
            name: "Synth Dreams",
            category: "MASSIVE X",
            price: "FREE"
        } 
    ]);
    return(
        <CartContext.Provider value={[cart, setCart]}>
            {props.children}
        </CartContext.Provider>
    );
}