import React, { useContext } from 'react';
import Card from '../ui/Card';
import Title from '../ui/Title';
import Filters from '../ui/Filters';
import { SearchContext } from '../../context/SearchContext';





function Store() {
  const [filter, setFilter] = useContext(SearchContext); 

  const products = {
    "1": {
      url: "https://dt7v1i9vyp3mf.cloudfront.net/styles/news_large/s3/imagelibrary/N/NativeInstrumentsMassiveX_01-9kyL90Cf9j6d9HzeOTId2Pga.kDmMrkL.jpg",
      name: "Synth Dreams",
      category: "MASSIVE X",
      price: "FREE"
    }, 
    "2": {
      url: "https://dt7v1i9vyp3mf.cloudfront.net/styles/news_large/s3/imagelibrary/N/NISymphonySeriesBrassCollection_01-Mp0P3AaUBzkcP5WkzWNdFmeN2CShJy3S.jpg",
      name: "Ethyrsun Theme",
      category: "ORCHESTRAL",
      price: "£9.99"
    }, 
    "3": {
      url: "https://www.native-instruments.com/typo3temp/pics/img-ce-kontakt-6_subpage_libraries_02_retromachines2-ebd15db8a9660b0b98645d4539f6ea40-m@2x.jpg",
      name: "Kontakt Template",
      category: "KONTAKT",
      price: "FREE"
    }, 
    "4": {
      url: "https://www.native-instruments.com/typo3temp/pics/img-welcome-hero-noire-welcome_1-7b27413c91f338b1ca45cb06680549ff-m@2x.jpg",
      name: "Nils Frahm",
      category: "PIANO NOIRE",
      price: "£2.00" 
    }
  }
  return (
    <section className="text-gray-800 bg-gray-100 body-font">
      <Title title="Store" subtitle={filter.toUpperCase()}/>
      <Filters />
        <div className="flex flex-wrap -m-4">
          <Card image={products["1"].url} name={products["1"].name} price={products["1"].price} category={products["1"].category} />
          <Card image={products["2"].url} name={products["2"].name} price={products["2"].price} category={products["2"].category} />
          <Card image={products["3"].url} name={products["3"].name} price={products["3"].price} category={products["3"].category} />
          <Card image={products["4"].url} name={products["4"].name} price={products["4"].price} category={products["4"].category} />
        </div>
    </section>
  );
}

export default Store;
