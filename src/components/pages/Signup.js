import React from 'react';
import Logo from '../../logo.png';
import { useHistory, useLocation, Redirect } from 'react-router-dom';

function Signup(props) {
  let history = useHistory();

  const authenticate = () => props.auth.authenticate(() => {
    history.push("/");
  });
  const signout = () => props.auth.signout(() => console.log("Signed out"));
  return (
    <section className="text-gray-700 body-font">
      <div className="container px-5 py-24 mx-auto flex flex-wrap items-center">
        <div className="lg:w-3/5 md:w-1/2 md:pr-16 lg:pr-0 pr-0">
          <h1 className="title-font font-bold text-3xl text-gray-900">Become a part of our community</h1>
          <p className="leading-relaxed mt-4">Have access to uploading tracks, being notified for new releases, and having a say on future tracks - as well as free access to all blog posts!</p>
        </div>
        <div className="lg:w-2/6 md:w-1/2 bg-gray-200 rounded-lg p-8 flex flex-col md:ml-auto w-full mt-10 md:mt-0">
          <h2 className="text-gray-900 text-lg font-medium title-font mb-5">Sign Up</h2>
          <input className="bg-white rounded border border-gray-400 focus:outline-none focus:border-teal-500 text-base px-4 py-2 mb-4" placeholder="Email" type="email" />
          <input className="bg-white rounded border border-gray-400 focus:outline-none focus:border-teal-500 text-base px-4 py-2 mb-4" placeholder="Password" type="password" />
          <button onClick={authenticate} className="text-white bg-teal-500 border-0 py-2 px-8 focus:outline-none hover:bg-teal-600 rounded text-lg mb-2">Sign Up</button>
          <button className="justify-center bg-gray-300 hover:bg-gray-400 text-gray-700 py-2 px-8 rounded inline-flex items-center">
            <img src={Logo} className="fill-current w-4 h-4 mr-2" />
            <span>Sign Up with Google</span>
          </button>
          <button href="#" onClick={signout} className="text-xs text-gray-500 mt-3">Log Out</button>
        </div>
      </div>
    </section>
  );
}

export default Signup;