import React, { useContext } from 'react';
import Title from '../ui/Title';
import CheckoutListItem from '../ui/CheckoutListItem';
import { CartContext } from '../../context/CartContext';

function Checkout() {
  const [cart, setCart] = useContext(CartContext);
  return (
    <section className="text-gray-700 body-font">
        <Title title="Checkout" subtitle="COMPLETE THIS ORDER" />
        <div className="lg:w-2/3 w-full mx-auto overflow-auto">
          <table className="table-auto w-full text-left whitespace-no-wrap">
            <thead>
              <tr>
                <th className="px-4 py-3 title-font tracking-wider font-medium text-gray-900 text-sm bg-gray-200 rounded-tl rounded-bl">Product</th>
                <th className="px-4 py-3 title-font tracking-wider font-medium text-gray-900 text-sm bg-gray-200">Size</th>
                <th className="px-4 py-3 title-font tracking-wider font-medium text-gray-900 text-sm bg-gray-200">Price</th>
              </tr>
            </thead>
            <tbody>
              <CheckoutListItem name={cart[0].name} price={cart[0].price} filesize={cart[0].filesiz} />
            </tbody>
          </table>
        </div>
        <div className="flex pl-4 mt-4 lg:w-2/3 w-full mx-auto">
          <button className="text-teal-500 inline-flex items-center md:mb-2 lg:mb-0">Learn More
            <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-4 h-4 ml-2" viewBox="0 0 24 24">
              <path d="M5 12h14M12 5l7 7-7 7"></path>
            </svg>
          </button>
          <button className="flex ml-auto text-white bg-teal-500 border-0 py-2 px-6 focus:outline-none hover:bg-teal-600 rounded">Proceed to payment</button>
        </div>
    </section>
  );
}

export default Checkout;
