import React from 'react';
import Warning from '../ui/Warning';

function Product() {
  return (
    <section className="text-gray-700 body-font overflow-hidden">
      <div className="lg:w-4/5 mx-auto flex flex-wrap">
        <img alt="ecommerce" className="lg:w-1/2 w-full lg:h-auto h-64 object-cover object-center rounded" src="https://dt7v1i9vyp3mf.cloudfront.net/styles/news_large/s3/imagelibrary/N/NativeInstrumentsMassiveX_01-9kyL90Cf9j6d9HzeOTId2Pga.kDmMrkL.jpg" />
        <div className="lg:w-1/2 w-full lg:pl-10 lg:py-6 mt-6 lg:mt-0">
          <h2 className="text-sm title-font text-gray-500 tracking-widest">MASSIVE X</h2>
          <h1 className="text-gray-900 text-3xl title-font font-bold mb-1">The Catalyst</h1>
          <p className="leading-relaxed mb-2">Fam locavore kickstarter distillery. Mixtape chillwave tumeric sriracha taximy chia microdosing tilde DIY. XOXO fam indxgo juiceramps cornhole raw denim forage brooklyn. Everyday carry +1 seitan poutine tumeric. Gastropub blue bottle austin listicle pour-over, neutra jean shorts keytar banjo tattooed umami cardigan.</p>
          <Warning title="Requires Massive X" description="This product requires the full version of Massive X to run" />
          <div className="flex mt-2 items-center pb-5 border-b-2 border-gray-200 mb-5">
          </div>
          <div className="flex">
            <span className="title-font font-medium text-2xl text-gray-900">£0.99</span>
            <button className="flex ml-auto text-white bg-teal-500 border-0 py-2 px-6 focus:outline-none hover:bg-teal-600 rounded">Add to cart</button>
          </div>
        </div>
      </div>
  </section>
  );
}

export default Product;
