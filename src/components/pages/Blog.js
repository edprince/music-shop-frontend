import React from 'react';
import Title from '../ui/Title';
import Bloglisting from '../ui/Bloglisting'

function Blog() {
  const images = [
    "https://dt7v1i9vyp3mf.cloudfront.net/styles/news_large/s3/imagelibrary/N/NativeInstrumentsMassiveX_01-9kyL90Cf9j6d9HzeOTId2Pga.kDmMrkL.jpg",
    "https://dt7v1i9vyp3mf.cloudfront.net/styles/news_large/s3/imagelibrary/N/NISymphonySeriesBrassCollection_01-Mp0P3AaUBzkcP5WkzWNdFmeN2CShJy3S.jpg",
    "https://www.native-instruments.com/typo3temp/pics/img-ce-kontakt-6_subpage_libraries_02_retromachines2-ebd15db8a9660b0b98645d4539f6ea40-m@2x.jpg",
    "https://www.native-instruments.com/typo3temp/pics/img-welcome-hero-noire-welcome_1-7b27413c91f338b1ca45cb06680549ff-m@2x.jpg"
  ];
  return (
    <section className="text-gray-700 body-font overflow-hidden">
      <Title title="Blog" subtitle="RECENT POSTS" />
      <Bloglisting category="PRODUCTION"/>
      <Bloglisting category="COMPOSITION"/>
      <Bloglisting category="COMPOSITION"/>
      <Bloglisting category="COMPOSITION"/>
    </section>
  );
}

export default Blog;
