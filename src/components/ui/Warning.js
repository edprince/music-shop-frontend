import React from 'react';

function ButtonGroup(props) {
  return (
    <div className="bg-orange-100 border-l-4 border-orange-500 text-orange-700 p-4" role="alert">
      <p className="font-bold">{props.title}</p>
      <p>{props.description}</p>
    </div>
  );
}

export default ButtonGroup;
