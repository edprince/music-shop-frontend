import React, { useContext } from 'react';
import { SearchContext } from '../../context/SearchContext';

function ButtonGroup() {
  const [filter, setFilter] = useContext(SearchContext);

  const filterAll = () => setFilter("all");
  const filterPaid = () => setFilter("paid");
  const filterFree = () => setFilter("free");

  return (
        <div className="mr-2">
          <button onClick={filterAll} className={((filter === "all") ? ("bg-teal-500 text-white ") : "bg-white text-gray-800") + "font-bold py-2 px-4 rounded-l focus:outline-none border-gray-400 border-l border-t border-b"}>
            All
          </button>
          <button onClick={filterFree} className={((filter === "free") ? ("bg-teal-500 text-white ") : "bg-white text-gray-800") + "font-bold py-2 px-4 focus:outline-none border-gray-400 border-t border-b"}>
            Free
          </button>
          <button onClick={filterPaid} className={((filter === "paid") ? ("bg-teal-500 text-white ") : "bg-white text-gray-800") + "font-bold py-2 px-4 rounded-r focus:outline-none border-gray-400 border-r border-t border-b"}>
            Paid
          </button>
        </div>
  );
}

export default ButtonGroup;
