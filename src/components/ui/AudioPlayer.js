import React from 'react';
import {
  Link
} from "react-router-dom";
import { useAudioPlayer } from "react-use-audio-player";


function AudioPlayer(props) {
  return (
      <AudioPlayerProvider>
          <AudioPlayer file="../../starry_sky.mp3" />
      </AudioPlayerProvider>
  );
}

export default AudioPlayer;