import React from 'react';

function StoreSearch() {
  return (
      <input className="leading-snug w-full bg-white rounded border border-gray-400 focus:outline-none focus:border-teal-500 text-base px-4 py-2" placeholder="Search" type="text" />
  );
}

export default StoreSearch;
