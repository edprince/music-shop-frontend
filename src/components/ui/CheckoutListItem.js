import React from 'react';
import {
  Link
} from "react-router-dom";

function CheckoutListItem(props) {
  return (
    <tr>
      <td className="border-t-2 border-gray-200 px-4 py-3">{props.name}</td>
      <td className="border-t-2 border-gray-200 px-4 py-3">{props.filesize}</td>
      <td className="border-t-2 border-gray-200 px-4 py-3 text-lg text-gray-900">{props.price}</td>
    </tr>
  );
}

export default CheckoutListItem;
