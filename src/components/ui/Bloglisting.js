import React from 'react';
import {
  Link
} from "react-router-dom";

function Bloglisting(props) {
  const images = [
    "https://dt7v1i9vyp3mf.cloudfront.net/styles/news_large/s3/imagelibrary/N/NativeInstrumentsMassiveX_01-9kyL90Cf9j6d9HzeOTId2Pga.kDmMrkL.jpg",
    "https://dt7v1i9vyp3mf.cloudfront.net/styles/news_large/s3/imagelibrary/N/NISymphonySeriesBrassCollection_01-Mp0P3AaUBzkcP5WkzWNdFmeN2CShJy3S.jpg",
    "https://www.native-instruments.com/typo3temp/pics/img-ce-kontakt-6_subpage_libraries_02_retromachines2-ebd15db8a9660b0b98645d4539f6ea40-m@2x.jpg",
    "https://www.native-instruments.com/typo3temp/pics/img-welcome-hero-noire-welcome_1-7b27413c91f338b1ca45cb06680549ff-m@2x.jpg"
  ];
  return (
        <div className="-my-8">
          <div className="py-10 flex flex-wrap md:flex-no-wrap">
            <div className="md:w-64 md:mb-0 mb-6 flex-shrink-0 flex flex-col">
            </div>
            <div className="md:flex-grow">
              <span className="tracking-widest font-medium title-font text-teal-500">{props.category}</span>
              <Link to="/blogpost"><h2 className="text-2xl font-semibold text-gray-900 title-font mt-2 mb-2">Bitters hashtag waistcoat fashion axe chia unicorn</h2></Link>
              <p className="leading-relaxed mb-1">Glossier echo park pug, church-key sartorial biodiesel vexillologist pop-up snackwave ramps cornhole. Marfa 3 wolf moon party messenger bag selfies, poke vaporware kombucha lumbersexual pork belly polaroid hoodie portland craft beer.</p>
              <span className="mt-1 text-gray-900 text-sm">12 Jun 2019</span>
            </div>
            <div className="md:w-64 md:mb-0 mb-6 flex-shrink-0 flex flex-col">
            </div>
          </div>
        </div>
  );
}

export default Bloglisting;
