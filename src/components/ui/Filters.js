import React from 'react';
import StoreSearch from './StoreSearch';
import Dropdown from './Dropdown';
import ButtonGroup from './ButtonGroup';

function Filters() {
  return (
       <div className="container mx-auto flex flex-wrap pb-5 pr-2 flex-col md:flex-row items-center">
          <a href="#" className="flex title-font font-medium items-center text-gray-900 mb-4 md:mb-0">
            <ButtonGroup />
          </a>
          <nav className="w-full sm:w-full md:w-1/2 md:mr-auto md:ml-4 md:py-1 md:pl-4 flex flex-wrap items-center text-base justify-center">
            <StoreSearch />
          </nav>
          <div className="inline-flex items-center mt-4 md:mt-0">
            <Dropdown />
          </div>
        </div> 

  );
}

export default Filters;
/*
    <div className="mb-4 w-full">
      <div className="flex flex-wrap items-center flex-wrap">
        <div className="mb-2 mr-2">
          <button className="bg-teal-500 hover:bg-gray-400 text-white font-bold py-2 px-4 rounded-l">
            All
          </button>
          <button className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4">
            Free
          </button>
          <button className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-r">
            Paid
          </button>
        </div>
        <div className="w-full sm:w-full md:w-1/2 lg:w-1/3 leading-snug">
          <StoreSearch />
        </div>
      </div>
      <div className="w-full sm:w-full md:w-1/2 lg:w-1/3 leading-snug">
        <Dropdown />
      </div>
    </div>
    */