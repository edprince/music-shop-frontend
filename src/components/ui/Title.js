import React from 'react';

function Title(props) {
  return (
      <div className="flex flex-col text-center w-full mb-10">
        <h2 className="text-xs text-teal-500 tracking-widest font-medium title-font mb-1">{props.subtitle}</h2>
        <h1 className="sm:text-3xl text-2xl font-bold title-font text-gray-900">{props.title}</h1>
      </div>
  );
}

export default Title;